# Using Reinforcement Learning to Construct Geometry Using Signed Distance Function Primitives

![image info](string2.png)

This repo provides code detailing the construction of simple geometric strings utilising implicit geometry - specifically using signed distance funciton primitives.
Using work provided by: https://github.com/fogleman/sdf
This code lays out a process to evolve a string of SDF primitives to form geometry. This 

## Requirements

Python >= 9.10
emvironment.yml

## Instructions
```
conda env create -f environment.yml
``` 
- Set target points for growth
- Set max number of primitives
- Set update period for policy
- Set max number of timesteps

![image info](string_truss.png)