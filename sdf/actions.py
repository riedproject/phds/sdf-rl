import functools
import numpy as np
import os

from sdf import mesh

# np.set_printoptions(threshold=sys.maxsize)

# Operations and transformations of sdf primitives. Based on https://github.com/fogleman/sdf

ORIGIN = np.array((0, 0, 0))

X = np.array((1, 0, 0))
Y = np.array((0, 1, 0))
Z = np.array((0, 0, 1))

UP = Z

# SDF Class

_ops = {}


class SDF3:
    def __init__(self, f):
        self.f = f

    def __call__(self, p):
        return self.f(p).reshape((-1, 1))

    def __getattr__(self, name):
        if name in _ops:
            f = _ops[name]
            return functools.partial(f, self)
        raise AttributeError

    def k(self, k=None):
        self._k = k
        return self

    def save(self, path, *args, **kwargs):
        return mesh.save(path, self, *args, **kwargs)


def sdf3(f):
    def wrapper(*args, **kwargs):
        return SDF3(f(*args, **kwargs))
    return wrapper


def op3(f):
    def wrapper(*args, **kwargs):
        return SDF3(f(*args, **kwargs))
    _ops[f.__name__] = wrapper
    return wrapper


# Helpers


def _length(a):
    return np.linalg.norm(a, axis=1)


def _normalize(a):
    return a / np.linalg.norm(a)


_min = np.minimum
_max = np.maximum

# Primitives


@sdf3
def box(size=1, center=ORIGIN, a=None, b=None):
    if a is not None and b is not None:
        a = np.array(a)
        b = np.array(b)
        size = b - a
        center = a + size / 2
        return box(size, center)
    size = np.array(size)

    def f(p):
        # print(f"p':{p}")
        q = np.abs(p - center) - size / 2

        return _length(_max(q, 0)) + _min(np.amax(q, axis=1), 0)
    return f


@sdf3
def capped_cylinder(a, b, radius):
    a = np.array(a)
    b = np.array(b)

    def f(p):
        ba = b - a
        pa = p - a
        baba = np.dot(ba, ba)
        paba = np.dot(pa, ba).reshape((-1, 1))
        x = _length(pa * baba - ba * paba) - radius * baba
        y = np.abs(paba - baba * 0.5) - baba * 0.5
        x = x.reshape((-1, 1))
        y = y.reshape((-1, 1))
        x2 = x * x
        y2 = y * y * baba
        d = np.where(
            _max(x, y) < 0,
            -_min(x2, y2),
            np.where(x > 0, x2, 0) + np.where(y > 0, y2, 0))
        return np.sign(d) * np.sqrt(np.abs(d)) / baba
    return f


# Positioning


@op3
def translate(other, offset):
    def f(p):
        return other(p - offset)
    return f


@op3
def union(a, *bs, k=None, blending_type="polynomial"):
    def f(p):
        d1 = a(p)
        # print(d1, file=open('union_a.txt', 'a'))
        for b in bs:
            d2 = b(p)
            # print(d2, file=open('union_b.txt', 'a'))
            K = k or getattr(b, '_k', None)
            if K is None:
                d1 = _min(d1, d2)
            elif blending_type == "polynomial":
                h = np.clip(0.5 + 0.5 * (d2 - d1) / K, 0, 1)
                m = d2 + (d1 - d2) * h
                d1 = m - K * h * (1 - h)
            elif blending_type == "exponential":
                res = np.exp2(-K * d1) + np.exp2(-K * d2)
                d1 = -np.log2(res) / K
            elif blending_type == "power":
                d1 = np.power(d1, K)
                d2 = np.power(d2, K)
                d1 = np.power((d1 * d2) / (d1 + d2), 1.0 / K)
            # print(d1, file=open('union.txt', 'a'))
        return d1
    return f


@op3
def rotate(other, angle, vector=Z):
    x, y, z = _normalize(vector)
    s = np.sin(angle)
    c = np.cos(angle)
    m = 1 - c
    matrix = np.array([
        [m*x*x + c, m*x*y + z*s, m*z*x - y*s],
        [m*x*y - z*s, m*y*y + c, m*y*z + x*s],
        [m*z*x + y*s, m*y*z - x*s, m*z*z + c],
    ]).T

    def f(p):
        return other(np.dot(p, matrix))
    return f

