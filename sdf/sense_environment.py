from skimage import measure

import multiprocessing
import numpy as np
import matplotlib.pyplot as plt


WORKERS = multiprocessing.cpu_count()
SAMPLES = 2 ** 22
BATCH_SIZE = 32


# Sense Environment - used to provide feedback to system


def _edge_to_point(sdf, a_x, a_y, a_z):  # Closest boundary of a primitive to XYZ location
    att_point = np.array([a_x, a_y, a_z])
    att_point = np.array([att_point])
    dist = sdf(att_point)
    # print(f'dist: {dist[0][0]}')

    return dist[0][0]


# Meshing Estimation
# Not used


def _cartesian_product(*arrays):
    la = len(arrays)
    dtype = np.result_type(*arrays)
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    # print(f'arr:{arr}')
    for i, a in enumerate(np.ix_(*arrays)):
        arr[...,i] = a
    return arr.reshape(-1, la)


def _estimate_bounds(sdf):  # Estimates the boundary of primitive and returns data similar to point cloud
    s = 8
    x0 = y0 = z0 = -1e9
    x1 = y1 = z1 = 1e9
    prev = None

    all_p_inside = []
    all_ds = []
    for i in range(32):
        X = np.linspace(x0, x1, s)
        Y = np.linspace(y0, y1, s)
        Z = np.linspace(z0, z1, s)
        d = np.array([X[1] - X[0], Y[1] - Y[0], Z[1] - Z[0]])
        threshold = np.linalg.norm(d) / 2
        if threshold == prev:
            break
        prev = threshold
        P = _cartesian_product(X, Y, Z)
        ds = sdf(P)
        all_ds.extend(ds.tolist())
        inside_shape_idxs = np.argwhere(ds <= 0)
        inside_p = P[inside_shape_idxs[:, 0]]
        all_p_inside.extend(inside_p.tolist())
        volume = sdf(P).reshape((len(X), len(Y), len(Z)))
        where = np.argwhere(np.abs(volume) <= threshold)
        x1, y1, z1 = (x0, y0, z0) + where.max(axis=0) * d + d / 2
        x0, y0, z0 = (x0, y0, z0) + where.min(axis=0) * d - d / 2

    return all_p_inside

# Checks for intersection for signed distance of point compared to primitive - input two primitives
def _intersection(sdf0, sdf1):
    intersect = False
    ps = _estimate_bounds(sdf0)
    d_list = []
    for row, points in enumerate(ps):
        d = _edge_to_point(sdf1, points[0], points[1], points[2])
        d_list.append(d)
        if d <= 0:
            # print(f'value:{d}')
            intersect = True
            return intersect, 0
    return intersect, min(d_list)


'''

def _estimate_bounds(sdf):
    s = 2
    x0 = y0 = z0 = -1e9
    x1 = y1 = z1 = 1e9
    prev = None

    all_p_inside = []
    all_ds = []
    for i in range(32):
        X = np.linspace(x0, x1, s)
        Y = np.linspace(y0, y1, s)
        Z = np.linspace(z0, z1, s)
        d = np.array([X[1] - X[0], Y[1] - Y[0], Z[1] - Z[0]])
        # print(d, file=open("d.txt", 'a'))
        threshold = np.linalg.norm(d) / 2
        if threshold == prev:
            break
        prev = threshold
        P = _cartesian_product(X, Y, Z)
        # print(f"P:{P}")
        # print(f'P.shape:{P.shape}')
        ds = sdf(P)
        all_ds.extend(ds.tolist())
        inside_shape_idxs = np.argwhere(ds <= 0.0)
        inside_p = P[inside_shape_idxs[:, 0]]
        all_p_inside.extend(inside_p.tolist())
        volume = sdf(P).reshape((len(X), len(Y), len(Z)))
        where = np.argwhere(np.abs(volume) <= threshold)
        x1, y1, z1 = (x0, y0, z0) + where.max(axis=0) * d + d / 2
        x0, y0, z0 = (x0, y0, z0) + where.min(axis=0) * d - d / 2

    return ((x0, y0, z0), (x1, y1, z1))

'''

