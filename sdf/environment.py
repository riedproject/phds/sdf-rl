import os
import random

from sdf import *
from sdf.sense_environment import _edge_to_point, _estimate_bounds, _intersection
import numpy as np
import random as rand
import gymnasium as gym
from gymnasium import spaces
from time import strftime, localtime
from datetime import datetime

truss_member = capped_cylinder(-1.5 * Z, 1.5 * Z, 0.2)
truss_unit = union(truss_member.rotate(30 * math.pi/180, vector=X).translate((0, 0.75, 0)),
                   truss_member.rotate(-30 * math.pi/180, vector=X).translate((0, -0.75, 0)),
                   truss_member.rotate(90 * math.pi/180, vector=X).translate((0, 0, -1.5)), k=0.3)
# Truss Unit made as example primitive

def set_targets(*target_points):
    target_points = target_points
    return target_points


def proximity_check(distance_list):  # Checks distance from sequential primitives, returns true if below threshold
    # for sublist in distance_list:
    #     for distances in sublist:
    #         if np.all(distances < 0.01):
    #             return True
    # print(f'prox_check: {min(distance_list)[0]}')
    if min(distance_list)[0] < 0.1:
        return True
    return False


def bounds_check(coordinate_list, boundary):  # Checks if primitive is within boundary of action space
    for points in coordinate_list:
        for value in points:
            if value > boundary or value < -boundary:
                return True
    return False


def lin_distance(point1, point2):     # Linear distance between 2 points
    x_com = (point2[0] - point1[0])**2
    y_com = (point2[1] - point1[1])**2
    z_com = (point2[2] - point1[2])**2
    dist = math.sqrt(x_com + y_com + z_com)
    return dist


class ShapeEnvironment(gym.Env):
    def __init__(self, target_points, num_prims, max_prims):
        super(ShapeEnvironment, self).__init__()

        # Environment Setup -- Default values for first run - most variables = 0, or empty
        self.current_step = 0
        self.count = 0
        self.target_points = target_points
        self.num_prims = num_prims
        self.primitives = []
        self.positions = []
        for prims in range(self.num_prims):
            self.primitives.append(box)
            self.positions.append([])
        self.max_prims = max_prims
        # self.exp = 1
        self.total_reward = 0
        self.num_intersections = 0
        self.prox_count = 0
        self.best_reward = []
        self.best_positions = []

        # Action and observation space
        self.action_space = spaces.Box(low=-10, high=10, shape=(3*self.max_prims,), dtype=np.float32)  # Box allows discrete and indescrete values. shape is 3 for XYZ axis
        self.observation_space = spaces.Box(low=-np.inf, high=np.inf, shape=(self.max_prims * len(self.target_points),),
                                            dtype=np.float32)

        # Output Printing
        # Note: Bug in current print. XYZ coordinates not printing zero values and labels for all primitives after the
        #       first - needs to be added manually
        self.spreadsheet_count = 0
        self.time = strftime('%d-%m-%Y_%H.%M.%S', localtime())
        self.path = f'{os.getcwd()}\\Intersect_{self.time}'
        os.mkdir(self.path)
        # print(f'Logging to: {self.path}')

        with open(f'{self.path}\\target_points.csv', 'a') as t:
            heading_list = []
            point_list = []
            for point in range(len(self.target_points)):
                heading_list.append(f'x{point},y{point},z{point}')
            t.write(str(heading_list).replace('[', '').replace(']', '').replace("'", "").replace(" ", ""))
            t.write('\n')
            for _, points in enumerate(target_points):
                point_list.append(f'{points[0]},{points[1]},{points[2]}')
            t.write(str(point_list).replace('[', '').replace(']', '').replace("'", ""))
            t.write('\n')

        with open(f'{self.path}\\xyz.csv', 'a') as f:
            p_heading = []
            for num in range(self.num_prims):
                p_heading.append(f'x{num},y{num},z{num}')
            f.write(str(p_heading).replace('[', '').replace(']', '').replace("'", "").replace(" ", ""))
            f.write('\n')

    def get_observation(self, obs_list):  # Compiles observation (List of distances from target point to primitives)
        obs = np.concatenate(obs_list)
        num_zeroes = (self.max_prims * len(self.target_points)) - len(obs)
        obs = np.pad(obs, (0, num_zeroes), mode='constant', constant_values=0)
        # print(f'observation: {obs}')
        return obs

    def reset(self, **kwargs): # Reset mid-run (called when system reaches end condition, or, boundary condition)

        self.current_step = 0
        self.primitives = []
        self.positions = []
        for prims in range(self.num_prims):  # Compiling list of primitives
            self.primitives.append(box)
            self.positions.append([])
        self.action_space = spaces.Box(low=-10, high=10, shape=(3*self.num_prims,), dtype=np.float32)   # 3D transform

        spawn_points = []
        for num in range(self.num_prims):  # Randomising primitive location around an origin
            # spawn_points.append(np.array((2*np.random.rand(1, 3))-1))
            points = [random.uniform(-1, 1), random.uniform(-1, 1), random.uniform(-1, 1)]
            spawn_points.append(points)
        self.positions = spawn_points

        primitives = []
        distance_list = []
        for num, prim in enumerate(self.primitives):  # 'spawning' primitives at start location
            primitives.append(prim.translate(spawn_points[num]))
        for _, _ in enumerate(self.target_points):  # Creating appropriate length list for distances
            distance_list.append([])
        for num, point in enumerate(self.target_points):
            for prim in range(len(primitives)):
                distance_list[num].append(_edge_to_point(primitives[prim], self.target_points[num][0],
                                                         self.target_points[num][1], self.target_points[num][2]))

        obs = self.get_observation(distance_list)
        # print(f'Observation: {obs}')
        # print(f'Obs_Shape: {obs.shape}')
        self.total_reward = 0  # Reset Global Values for recording
        self.num_intersections = 0
        # print(f'End of reset')
        return obs, {}

    def step(self, action):  # Action = Action space output - transformation values
        # print(f'start of step')
        done = False
        scope_limit = False

        primitives = []
        # print(f'action: {action}')
        for num, prim in enumerate(self.primitives):
            trans = action[num*3:(num*3)+3]
            self.positions[num] = np.add(self.positions[num], trans).tolist()
            primitives.append(prim.translate(self.positions[num]))

        distance_list = []
        for num, point in enumerate(self.target_points):
            distance_list.append([])
            # print(f'list: {distance_list}')
            for prim in range(len(primitives)):
                distance_list[num].append(abs(_edge_to_point(primitives[prim], self.target_points[num][0],
                                                             self.target_points[num][1], self.target_points[num][2])))

        origin_distance_list = []
        for prim in range(len(primitives)):
            origin_distance_list.append(abs(_edge_to_point(primitives[prim], 0, 0, 0)))


        """
        Old Intersection Test - Slow
        # intersect, min_d = _intersection(primitives[0], primitives[1])
        # if intersect is True:
        #     self.num_intersections += 1
        intersects = []
        # inter_dists = []
        # if len(primitives) >= 2:
        #     for num, prim1 in enumerate(primitives):
        #         intersects.append([])
        #         for inter, prim2 in enumerate(primitives):
        #             if inter == num:
        #                 intersect = None
        #             else:
        #                 intersect, min_d = _intersection(prim1, prim2)
        #                 inter_dists.append(min_d)
        #             intersects[num].append(intersect)
        # print(f'min_d{inter_dists}')
        # print(f'intersects{intersects}')
        #print(f'Intersect List: {intersects}')

        # min_list = []
        # for num, ds in enumerate(distance_list):
        #     min_list.append(min(ds))
        # print(f'mins:{min_list}')

        # if len(primitives) >= 2:
        #     reward = -1 * (sum(min_list) + min(inter_dists))
        # else:
        #     reward = -1 * sum(min_list)
        """

        reward = 0
        if self.num_prims == 1:  # Reward Metric - Aims to minimise distance between subsequent primitives and targets
            # reward += min(distance_list)[0]
            reward += distance_list[0][0]
            # for num, point in enumerate(self.target_points):
                # for prim in range(self.num_prims):
                #     reward += min(distance_list[num])
                    # reward += distance_list[num][prim]
        else:
            for prim in range(self.num_prims):
                if prim == 0:
                    reward += distance_list[0][prim]
                if prim >= 1:
                    distance = lin_distance(self.positions[prim], self.positions[prim - 1])
                    if distance >= 2.5:
                        reward += distance * 2.5 + distance_list[1][prim]/5
                    else:
                        intersects, min_d = _intersection(self.primitives[prim], self.primitives[prim - 1])
                        if intersects is True:
                        # if intersects is True or min_d <= 0.1:
                    # reward += min(distance_list[num])
                            self.prox_count += 1
                            reward += distance_list[1][prim] / 20
                        else:
                            reward += distance_list[1][prim]/5 + min_d * 2

        reward = -1 * reward

        # Saving best reward (total)
        if self.num_prims >= len(self.best_reward):
            # Extend the list with None values up to the desired index
            self.best_reward.extend([None] * (self.num_prims - len(self.best_reward)))
        if self.num_prims >= len(self.best_positions):
            # Extend the list with None values up to the desired index
            self.best_positions.extend([None] * (self.num_prims - len(self.best_positions)))
        # print(self.best_reward)
        # print(self.best_positions)
        if self.best_reward[self.num_prims-1] is None:
            self.best_reward[self.num_prims-1] = -1e9
        if reward > self.best_reward[self.num_prims-1]:
            self.best_reward[self.num_prims-1] = reward
            self.best_positions[self.num_prims-1] = self.positions

        # Metric for adding new primitives to system
        if self.num_prims == 1:
            done = proximity_check(distance_list)
            if done:
                print(done)
                self.prox_count += 1
        else:
            stg1 = proximity_check(distance_list)
            if stg1:
                self.prox_count += 1
                # if intersects[self.num_prims-1][self.num_prims-2] is True or inter_dists[-1] <= 0.01:
                #     done = True
                #     self.prox_count += 1
                if self.prox_count > 1:
                    done = True
                else:
                    self.prox_count = 0
            else:
                self.prox_count = 0

        if self.prox_count == self.num_prims and self.num_prims < self.max_prims:
            self.num_prims += 1
        self.prox_count = 0

        # if distance_list[0][-1] < 0.01:
        #     finish_time = datetime.now() - datetime.strptime(self.time, '%d-%m-%Y_%H.%M.%S')
        #     finish_log = open(f'{self.path}\\Finish Time.txt', 'a')
        #     print(f'No. of Primitives: {self.num_prims} - {finish_time}', file=finish_log)
        #     finish_log.close()
        # scope_limit = bounds_check(self.positions, 10)

        # print(f'done:{done}')

        obs = self.get_observation(distance_list)

        self.total_reward += reward
        # print(f'Total Reward: {self.total_reward}')
        self.current_step += 1  # Necessary for printing outputs
        max_step = 10

        if self.current_step > max_step:
            scope_limit = True
        if self.current_step == 1:
            print(f'reset?')

        if done or scope_limit is True:
            self.count += 1
            if self.count / 10 == 1:
                self.spreadsheet_count += 1
                with open(f'{self.path}\\xyz.csv', 'a') as f:
                    xyz = []
                    for prim in range(self.num_prims):
                        print(f'prim_len:{self.num_prims}, pos_len: {(len(self.positions)):}')
                        if self.num_prims > len(self.positions):
                            print(f'positions pre extend:{self.positions}')
                            self.positions.extend([[0, 0, 0]] * (self.num_prims - len(self.positions)))
                        # print(f'range_prims: {range(self.num_prims)}, range_positions: {range(len(self.positions[prim]))}')
                        print(f'positions post extend:{self.positions}')
                        print(self.positions[prim])
                        for num in range(len(self.positions[prim])):
                            xyz.append(self.positions[prim][num])
                    f.write(str(xyz).replace('[', '').replace(']', '').replace("'", "").replace(" ", ""))
                    f.write('\n')
                self.count = 1

        return obs, reward, done, scope_limit, {'total_reward': self.total_reward}


"""
        # exp = self.exp
        # if intersect is False:
        #     reward = -1e1 * np.abs(min_d) - (sum(min_list))
        # else:
        #     reward = -1 * (sum(min_list))
        #     # print(f'reward:{reward}')


            with open(f'{self.path}\\distance1_{len(self.target_points)}points.csv', 'a') as g:
                d_heading = ['n']
                for _, target_point in enumerate(self.target_points):
                    d_heading.append(f',d1_{_}')
                heading_str = ''
                for x in d_heading:
                    heading_str += ''+x
                g.write(heading_str)
                g.write('\n')
            with open(f'{self.path}\\distance2_{len(self.target_points)}points.csv', 'a') as g:
                d_heading = ['n']
                for _, target_point in enumerate(self.target_points):
                    d_heading.append(f',d2_{_}')
                heading_str = ''
                for x in d_heading:
                    heading_str += ''+x
                g.write(heading_str)
                g.write('\n')
                
                            with open(f'{self.path}\\distance1_{len(self.target_points)}points.csv', 'a') as g:
                # ds = f'{self.spreadsheet_count}, {self.distances[-3][0][0]}, {self.distances[-2][0][0]}, {self.distances[-1][0][0]}'
                d_list = [f'{self.spreadsheet_count}']
                for _, distance in enumerate(self.distances_1):
                    d_list.append(f',{distance}')
                d_str = ''
                for x in d_list:
                    d_str += ''+x
                # print(d_str)
                g.write(d_str)
                g.write('\n')
            with open(f'{self.path}\\distance2_{len(self.target_points)}points.csv', 'a') as g:
                # ds = f'{self.spreadsheet_count}, {self.distances[-3][0][0]}, {self.distances[-2][0][0]}, {self.distances[-1][0][0]}'
                d_list = [f'{self.spreadsheet_count}']
                for _, distance in enumerate(self.distances_2):
                    d_list.append(f',{distance}')
                d_str = ''
                for x in d_list:
                    d_str += ''+x
                # print(d_str)
                g.write(d_str)
                g.write('\n')
"""