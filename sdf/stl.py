import numpy as np
import struct
import sys

# np.set_printoptions(threshold=sys.maxsize)

def write_binary_stl(path, points):
    n = len(points) // 3

    points = np.array(points, dtype='float32').reshape((-1, 3, 3))
    # print(points, file=open('union_points.txt', 'a'))
    normals = np.cross(points[:,1] - points[:,0], points[:,2] - points[:,0])
    # print(normals, file=open('gyroid.txt', 'a'))
    normals /= np.linalg.norm(normals, axis=1).reshape((-1, 1))

    dtype = np.dtype([
        ('normal', ('<f', 3)),
        ('points', ('<f', (3, 3))),
        ('attr', '<H'),
    ])

    a = np.zeros(n, dtype=dtype)
    a['points'] = points
    a['normal'] = normals

    with open(path, 'wb') as fp:
        fp.write(b'\x00' * 80)
        fp.write(struct.pack('<I', n))
        fp.write(a.tobytes())
