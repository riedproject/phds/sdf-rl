import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import pandas

target_points = 5

points = pandas.read_csv('xyz_2points.csv')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x1 = points['x1'].values
y1 = points['y1'].values
z1 = points['z1'].values
x2 = points['x2'].values
y2 = points['y2'].values
z2 = points['z2'].values
col = points['col'].values

ax.scatter(x1, y1, z1, c=col, marker='o', cmap='Greys')
ax.scatter(x2, y2, z2, c=col, marker='o', cmap='Blues')
ax.scatter(0, 10, 0, c='red', marker='x')
ax.scatter(0, -10, 0, c='red', marker='x')
ax.scatter(10, 0, 0, c='red', marker='x')
ax.scatter(0, 7.5, 0, c='red', marker='x')
ax.scatter(0, -3, 0, c='red', marker='x')

plt.show()


# trend = pandas.read_csv('distance_5points.csv')
#
# fig_trend = plt.figure()
#
# n = trend['n'].values
# d1 = trend['d0'].values
# d2 = trend['d1'].values
# d3 = trend['d2'].values
# d4 = trend['d3'].values
# d5 = trend['d4'].values
#
# t1 = np.polyfit(n, d1, 5)
# p1 = np.poly1d(t1)
#
# t2 = np.polyfit(n, d2, 5)
# p2 = np.poly1d(t2)
#
# t3 = np.polyfit(n, d3, 5)
# p3 = np.poly1d(t3)
#
# t4 = np.polyfit(n, d4, 5)
# p4 = np.poly1d(t4)
#
# t5 = np.polyfit(n, d5, 5)
# p5 = np.poly1d(t5)
#
# # plt.scatter(n, d1)
# # plt.scatter(n, d2)
# # plt.scatter(n, d3)
# # plt.scatter(n, d4)
# # plt.scatter(n, d5)
#
# plt.plot(n, p1(n), c='red', label=1)
# plt.plot(n, p2(n), c='blue', label=2)
# plt.plot(n, p3(n), c='yellow', label=3)
# plt.plot(n, p4(n), c='green', label=4)
# plt.plot(n, p5(n), c='purple', label=5)
# plt.legend()
#
# plt.show()
#
#
# # n = trend['n'].values
# # target_dict = {}
# # var_list = []
# # plot_list = []
# #
# # for num in range(target_points):
# #     target_dict[f'd{num + 1}'] = f'd{num}'
# #     var_list.append = f'd{num+1}'
# #     plot_list.append = f'd{num}'
# # print(target_dict)
# # d1 = trend['d0'].values
# # d2 = trend['d1'].values
# # d3 = trend['d2'].values
# # d4 = trend['d3'].values
# # d5 = trend['d4'].values
#
