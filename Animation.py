import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from matplotlib.animation import ArtistAnimation
from matplotlib.animation import PillowWriter
import matplotlib as mpl
import pandas as pd

# Outputs training data as gif
# Read data from CSV
path = 'C:\\Users\\40200281\\Documents\\SDF\\Non_evodevo\\New_base\\v. 0.06 - alt.2'
folder = 'Intersect_17-04-2024_15.36.44'
d_xyz = pd.read_csv(f'{path}\\{folder}\\xyz.csv')
d_tp = pd.read_csv(f'{path}\\{folder}\\target_points.csv')

writer = PillowWriter(20, metadata=dict(artist='JonathanB'), bitrate=1800)

num_prims = d_xyz.shape[1] // 3
num_tp = d_tp.shape[1] // 3

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlim3d(-10, 10)
ax.set_ylim3d(-10, 10)
ax.set_zlim3d(-10, 10)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.scatter(0, 0, 0, c='black', marker='x')
targets = []

ax2 = ax


for point in range(num_tp):
    targets.append(ax.scatter([], [], [], c='red', marker='x'))
    targets[point]._offsets3d = (d_tp[f'x{point}'], d_tp[f'y{point}'], d_tp[f'z{point}'])


scatters = []
for prim in range(num_prims):
    scatters.append(ax.scatter([], [], [], c=plt.cm.plasma(prim/num_prims)))

step = 10  # Plot every 10th point


def init():
    for prim in range(num_prims):
        scatters[prim]._offsets3d = ([], [], [])
    return scatters


def update(frame):
    # Update the scatter plot data for each frame
    start_index = frame * step
    end_index = min((frame + 1) * step, len(d_xyz[f'x0'].values))

    for prim in range(num_prims):
        scatters[prim]._offsets3d = (d_xyz[f'x{prim}'].values[start_index:end_index],
                                     d_xyz[f'y{prim}'].values[start_index:end_index],
                                     d_xyz[f'z{prim}'].values[start_index:end_index])


    return scatters


num_frames = (len(d_xyz[f'x0'].values) + step - 1) // step  # Calculate the total number of frames

animation = FuncAnimation(fig, update, frames=num_frames, init_func=init, interval=100)
ax.view_init(elev=0, azim=180)
# animation.save(f'{path}\\{folder}\\y_view.gif', writer='pillow')
ax.view_init(elev=0, azim=270)
# animation.save(f'{path}\\{folder}\\x_view.gif', writer='pillow')
ax.view_init(elev=30, azim=225)
# animation.save(f'{path}\\{folder}\\iso_view.gif', writer='pillow')


plt.show()












'''''''''
# Create an empty scatter plot for each group to be updated
def create_scatter(ax, num_groups):
    scatter_plots = []
    for _ in range(num_groups):
        scatter_plot = ax.scatter([], [], [], marker='o')
        scatter_plots.append(scatter_plot)
        print(f'scatter_plot:{scatter_plots}')
    return scatter_plots


def init(scatter_plots):
    for i, scatter_plot in enumerate(scatter_plots):
        x = group_data[i][0][:1]  # Take the first element
        y = group_data[i][1][:1]
        z = group_data[i][2][:1]

        scatter_plot._offsets3d = (x, y, z)

    return scatter_plots


def update(frame):
    # Update the scatter plot data for each frame

    start_index = frame * step
    end_index = min((frame + 1) * step, len(group_data[0][0]))
    print(f'start:{start_index}, end:{end_index}')

    for i, scatter_plot in enumerate(scatter_plots):
        x = [val for sublist in group_data[i][0][start_index:end_index] for val in sublist]
        y = [val for sublist in group_data[i][1][start_index:end_index] for val in sublist]
        z = [val for sublist in group_data[i][2][start_index:end_index] for val in sublist]
        print(f'x,y,z : {x} {y} {z}')
        scatter_plot._offsets3d = (x, y, z)

    return scatter_plots



# Extract x, y, z data for each group
num_groups = len(d_xyz.columns) // 3  # Assuming each group has columns x, y, z
print(f'num_groups: {num_groups}')

group_data = [d_xyz.iloc[:, i:i+3].values for i in range(0, len(d_xyz.columns), 3)]
print(f'group_data: {group_data}')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_xlim3d(-10, 10)
ax.set_ylim3d(-10, 10)
ax.set_zlim3d(-10, 10)


step = 100  # Plot every 100th point


scatter_plots = create_scatter(ax, num_groups)  # Create scatter plots
init(scatter_plots)  # Initialize the scatter plots

num_frames = max((len(group_data[0][0]) - 1) // step + 1, 1)  # Calculate the total number of frames

animation = FuncAnimation(fig, update, frames=num_frames, init_func=lambda: init(scatter_plots), interval=100)

plt.show()
'''''''''

'''''''''

heading = d_xyz.axes[1]
print((len(heading)-1)/2)

# for i  in range(int((len(heading)-1)/2)):
# #     x =

x1_data = d_xyz[f'x1'].values
y1_data = d_xyz['y1'].values
z1_data = d_xyz['z1'].values

x2_data = d_xyz['x2'].values
y2_data = d_xyz['y2'].values
z2_data = d_xyz['z2'].values

'''''''''

'''''''''
# Create an empty scatter plot to be updated
sc_1 = ax.scatter([], [], [], c='blue')
sc_2 = ax.scatter([], [], [], c='orange')
'''''''''

'''''''''
    sc_1._offsets3d = ([], [], [])  # Initialize with empty data
    sc_2._offsets3d = ([], [], [])  # Initialize with empty data
    sc_1._offsets3d = ax.scatter(5.5, 8.9, -5, c='red', marker='x')
    sc_1._offsets3d = ax.scatter(7, -4, 2.3, c='red', marker='x')
    sc_1._offsets3d = ax.scatter(0, 0, 0, c='green', marker='x')
    # sc._offsets3d = ax.scatter(10, 0, 0, c='red', marker='x')
    # sc._offsets3d = ax.scatter(0, 7.5, 0, c='red', marker='x')
    # sc._offsets3d = ax.scatter(0, -3, 0, c='red', marker='x')
    return sc_1, sc_2
'''''''''


'''''''''
    sc_1._offsets3d = (x1_data[start_index:end_index], y1_data[start_index:end_index], z1_data[start_index:end_index])
    sc_2._offsets3d = (x2_data[start_index:end_index], y2_data[start_index:end_index], z2_data[start_index:end_index])
    return sc_1, sc_2
'''''''''


"""""
def update(frame):
    # Update the scatter plot data for each frame
    step = 1000
    sc._offsets3d = (x_data[:frame + 1:step], y_data[:frame + 1:step], z_data[:frame + 1:step])
    return sc,


animation = FuncAnimation(fig, update, frames=len(x_data), init_func=init, interval=0.001)

plt.show()

    sc._offsets3d = ax.scatter(0, 10, 0, c='red', marker='x')
    sc._offsets3d = ax.scatter(0, -10, 0, c='red', marker='x')
    sc._offsets3d = ax.scatter(10, 0, 0, c='red', marker='x')
    sc._offsets3d = ax.scatter(0, 7.5, 0, c='red', marker='x')
    sc._offsets3d = ax.scatter(0, -3, 0, c='red', marker='x')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Create an empty scatter plot to be updated
sc = ax.scatter([], [], [])
x_array = []
y_array = []
z_array = []

def init():
    sc._offsets3d = (x_array, y_array, z_array)  # Initialize with empty data
    return sc,


def update(frame):
    # Update the scatter plot data for each frame
    # sc._offsets3d = ([x_data[frame]], [y_data[frame]], [z_data[frame]])
    x_array.append(x_data[frame])
    y_array.append(y_data[frame])
    z_array.append(z_data[frame])
    sc._offsets3d = (x_array, y_array, z_array)
    return sc,


animation = FuncAnimation(fig, update, frames=len(x_data), init_func=init, interval=200, blit=True)

plt.show()

"""""
