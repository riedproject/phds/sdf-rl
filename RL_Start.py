import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt
from sdf import *
from stable_baselines3 import PPO
from stable_baselines3.common.callbacks import BaseCallback
from stable_baselines3.common.vec_env import DummyVecEnv
from sdf.environment import ShapeEnvironment
from sdf.environment import set_targets
import os

path = os.getcwd()


time_steps = 150000  # No. of time steps run 500000
n_per_step = 64      # No. of time steps per policy update

num_prims = 1        # Starting no. of primitives
max_prims = 5        # Ending no. of primitives

set_targets = set_targets(np.array([10, 10, 10]),   # NP array of target points
                          np.array([10, 0, 10]))
                          # np.array([10, -5, 7]))
                          # np.array([0, -10, 6]))



class Policy(nn.Module):
    def __init__(self, input_size, output_size):
        super(Policy, self).__init__()
        self.fc = nn.Linear(input_size, output_size)

    def forward(self, x):
        return self.fc(x)


class TensorboardCallback(BaseCallback):
    """
    Custom callback for plotting additional values in tensorboard.
    For tensorboard callback use command prompt: tensorboard --logdir ./primitive_tensorboard/PPO_New
    """

    def __init__(self, verbose=0):
        super(TensorboardCallback, self).__init__(verbose)
        self.img_interval = 50
        self.step_count = 0
        self.best_reward = float('-inf')
        self.best_timestep = 0
        self.current_prim = 1

    def _on_step(self) -> bool:
        self.step_count += 1
        self.logger.record('reward', self.training_env.get_attr('total_reward')[0])
        self.logger.record('num_intersections', self.training_env.get_attr('num_intersections')[0])
        print(f"num_prims: {self.training_env.get_attr('num_prims')}")
        self.logger.record('num_primitives', self.training_env.get_attr('num_prims')[0])

        """
        Real-Time 3D plot with Tensorflow  -- very slow
        
        # if self.current_prim == primitive_number:
        #     print(f'no idea:{self.model.ep_info_buffer}')
        #     if self.model.ep_info_buffer:
        #         current_reward = self.model.ep_info_buffer[0].get('r', 0)
        #         if current_reward > self.best_reward:
        #             self.best_reward = current_reward
        #             self.best_timestep = self.num_timesteps
        #
        #             self.model.save(f"best_model_{primitive_number}_prims")
        # else:
        #     self.current_prim = primitive_number
        # if self.step_count % self.img_interval == 0:
        #     fig = plt.figure()
        #     ax = fig.add_subplot(111, projection='3d')
        #     positions = self.training_env.get_attr('positions')[0]
        #     print(f'positions: {positions}')
        #     for num, point in enumerate(positions):
        #         x, y, z = point  # Extract (x, y, z) coordinates
        #         ax.scatter(x, y, z, c=plt.cm.plasma(num / len(positions)))
        #     for target in set_targets:
        #         x, y, z = target
        #         ax.scatter(x, y, z, color='red')
        #     ax.scatter(0, 0, 0, color='black')
        #     ax.set_xlabel('X')
        #     ax.set_ylabel('Y')
        #     ax.set_zlabel('Z')
        #     self.logger.record('Position', Figure(fig, close=True), exclude=("stdout", "log", "json", "csv"))
        """

        return True


target_points = set_targets
env = DummyVecEnv([lambda: ShapeEnvironment(target_points, num_prims, max_prims)])
# env.action_space.seed(123)
callbacks = [TensorboardCallback()]
model = PPO('MlpPolicy', env, verbose=1, n_steps=n_per_step, tensorboard_log=f"./primitive_tensorboard/PPO_New/")
model.learn(total_timesteps=time_steps, callback=callbacks)  # 500000
best_positions = env.get_attr('best_positions')
print(f'best_positions: {best_positions[0][-1]}')
model.save("ppo_translate")  # Saves policy for future use

best_model = model.load("ppo_translate")  # Calls policy
positions = env.get_attr('positions')
prim = box(1)

truss_member = capped_cylinder(-1.5 * Z, 1.5 * Z, 0.2)
truss_unit = union(truss_member.rotate(30 * math.pi/180, vector=X).translate((0, 0.75, 0)),
                   truss_member.rotate(-30 * math.pi/180, vector=X).translate((0, -0.75, 0)),
                   truss_member.rotate(90 * math.pi/180, vector=X).translate((0, 0, -1.5)), k=0.3)

output = union(box(0.25), box(0.25).translate((10,10,10)))
for num, translate in enumerate(positions[0]):
    output = union(output, prim.translate((translate[0], translate[1], translate[2])))
    print(f'num{num}')
    print(f'translate:{translate}')
    print(positions[0])
output.save('output.stl')

output = union(box(0.25), box(0.25).translate((10, 10, 10)), box(0.25).translate((10, 0, 10)))
for num, translate in enumerate(best_positions[0][-1]):
    output = union(output, truss_unit.rotate(math.pi * num, vector=X).translate((translate[0], translate[1], translate[2])))
    print(f'num{num}')
    print(f'translate:{translate}')
    print(positions[0])
output.save('output_test1.stl', step=0.01)

output = union(box(0.25), box(0.25).translate((10, 10, 10)), box(0.25).translate((10, 0, 10)))
for num, translate in enumerate(best_positions[0][-2]):
    output = union(output, truss_unit.rotate(math.pi * num, vector=X).translate((translate[0], translate[1], translate[2])))
    print(f'num{num}')
    print(f'translate:{translate}')
    print(positions[0])
output.save('output_test2.stl', step=0.01)

env.close()

# obs = env.reset()
# for i in range(100):
#     with open(f'xyz_{len(target_points)}points.csv', 'a') as f:
#         f.write('\n')
#     action, _ = model.predict(obs)
#     obs, rewards, done, info = env.step(action)
#     if done:
#         obs = env.reset()
# obs = env.reset()
